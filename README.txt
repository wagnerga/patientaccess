#### Database, WebApp, and WebAPI must all reside on same server to make this work as easily as possible.

## Create a new database named PatientAccess.

## Execute the PatientAccess.sql script against the PatientAccess database to create the schema.

    Only 1 table named Patient should be created.

## Modify the PatientAccessAPI Web.config to point at the PatientAccess database created above.

    The <connectionStrings></connectionStrings> PatientAccessEntites needs to point to the correct "data source".

    The connection string currently assumes integrated security = true, so the database user needs needs window domain access.

## Run PatientAccessAPI in Visual Studio.

    A browser should load with URL http://localhost:53595.

## Install Node JS if it isn't already installed. (https://nodejs.org/en/download/)

## Install Node JS modules with a new admin console from the PatientAccessWeb directory.

    npm install

## Run the Webpack development server from the PatientAccessWeb directory.

    npm run dev

    The site URL will be printed out in the console. This should be URL http://localhost:8082.

## Copy and paste the site URL from above and open in a new browser, preferrably Chrome.