const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  devtool: 'cheap-module-source-map',

  devServer: {
    port: 8082,
    contentBase: path.resolve(__dirname, './app/static')
  },

  entry: {
    app: ['./app/app.js']
  },

  module: {
    rules: [
      {
        test: /jquery\.js$/,
        use: [
          'expose-loader?jQuery',
          'expose-loader?$'
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules|\.exp\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['es2015'],
              plugins: ['transform-object-rest-spread', 'syntax-dynamic-import']
            }
          }
        ]
      },
      {
        test: /\.exp\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['es2015'],
              plugins: ['add-module-exports']
            }
          }
        ]
      },
      {
        test: /\.(png|jpe?g|gif|woff|woff2|ttf|eot|otf|svg)($|\?)/,
        exclude: /\/app\/static\/.+\.svg$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: 'assets/[hash].[ext]'
          }
        }
      },
      {
        test: /\.html$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'html-loader',
            options: { interpolate: true }
          },
          'markup-inline-loader'
        ]
      },
      {
        test: /\.(less|css)$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'less-loader']
        })
      }
    ]
  },

  output: {
    path: path.resolve(__dirname, './build'),
    filename: 'bundle.js'
  },

  plugins: [
    new ExtractTextPlugin('[name].css'),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'vendor.js',
      minChunks(module) {
        return module.context && module.context.indexOf('node_modules') >= 0;
      }
    }),
    new HtmlWebpackPlugin({
      hash: true,
      template: './app/index.html'
    }),
    new webpack.ProvidePlugin({
      'app': 'app.exp'
    })
  ],

  resolve: {
    modules: [
      path.resolve('./app'),
      'node_modules'
    ]
  }
};