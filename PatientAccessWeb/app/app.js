﻿import './registration';
import './patientView';
import './filters';
import './dataServices';
import './app.less';

app.config([
    '$stateProvider', '$urlRouterProvider', '$urlMatcherFactoryProvider',
    function ($stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider) {
        $urlMatcherFactoryProvider.strictMode(false);
        $urlRouterProvider.otherwise('/registration');

        $stateProvider
            .state({
                name: 'registration',
                url: '/registration',
                template: '<registration></registration>'
            }).state({
                name: 'patientView',
                url: '/patientView/{patientId}',
                template: '<patient-view></patient-view>'
            });
    }
]);