﻿app.factory('patientDataService',
    [
        '$http', ($http) => {
            const url = 'http://localhost:53595/api/patients/';

            return {
                createPatient: (patient, onComplete) => {
                    $http.post(url, JSON.stringify(patient))
                        .then((response) => {
                            onComplete(response);
                        });
                },
                getPatients: (onComplete) => {
                    $http.get(url)
                        .then((response) => {
                            onComplete(response);
                        });
                },
                getPatient: (id, onComplete) => {
                    $http.get(url + id)
                        .then((response) => {
                            onComplete(response);
                        });
                },
                updatePatient: (patient) => {
                    $http.put(url + patient.Id, JSON.stringify(patient));
                },
                deletePatient: (id, onComplete) => {
                    $http.delete(url + id).then(() => {
                        onComplete();
                    });
                }
            }
        }
    ]);