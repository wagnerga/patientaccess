﻿import template from './registration.html';
import _ from 'lodash';

const controller = [
    '$scope', 'patientDataService',
    function ($scope, patientDataService) {
        const $ctrl = this;

        patientDataService.getPatients((response) => {
            $ctrl.patients = response.data;
        });

        $ctrl.createPatient = (patient) => {
            if (patient == null)
                return;

            patientDataService.createPatient(patient,
                (response) => {
                    $ctrl.patients.push(response.data);
                    $scope.newPatient = null;
                });
        };

        $ctrl.updatePatient = (patient) => {
            patientDataService.updatePatient(patient);
        };

        $ctrl.deletePatient = (id) => {
            patientDataService.deletePatient(id,
                () => {
                    _.remove($ctrl.patients, { Id: id });
                });
        };

        $scope.$watch($ctrl.patients, () => { });
    }
];

app.component('registration',
    {
        bindings: {
        },
        controller: controller,
        template: template
    });