﻿import template from './states.html';

const controller = [
    '$scope', '$element',
    function ($scope, $element) {
        const sel = $element[0].children[0];

        for (let i = 0; i < sel.options.length; i++) {
            if (sel.options[i].value === this.selectedValue) {
                sel.options[i].selected = true;
                break;
            }
        }

        this.onChange = (selectedValue) => {
            this.selectedValue = selectedValue;
        }
    }
];

app.component('states',
    {
        bindings: {
            selectedValue: '='
        },
        controller: controller,
        template: template
    });