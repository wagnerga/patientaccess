import angular from 'angular';
import '@uirouter/angularjs';

/* This is exported to all other modules as "app". */

export default angular.module('patientAccessApp', [
  'ui.router'
]);