﻿import template from './patientView.html';

const controller = [
    '$scope', '$stateParams', 'patientDataService',
    function ($scope, $stateParams, patientDataService) {
        const $ctrl = this;

        patientDataService.getPatient($stateParams.patientId,
            (response) => {
                $ctrl.patient = response.data;
            });
    }
];

app.component('patientView',
    {
        bindings: {
        },
        controller: controller,
        template: template
    });